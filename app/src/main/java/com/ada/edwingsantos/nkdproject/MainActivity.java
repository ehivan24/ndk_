package com.ada.edwingsantos.nkdproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("hello");
    }
    private native String getMsgFromJni();
    private native int NativeAdditon(int a, int b);
    private native int MemoryAllocation(int a, int b);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv = (TextView)findViewById(R.id.text_view);
        String msgFromJni =  getMsgFromJni();
        if (msgFromJni != null) {

            tv.setText(msgFromJni);
        }
    }


    /**
     * Generates a Toast message
     * @param message String to display on the toast message
     */
    private void createToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void backwardsControls(View view) {
        createToast("Clicked Backward");
    }

    public void forwardsControls(View view) {
        createToast("Clicked Forward");
    }

    public void stopControls(View view) {
        createToast("Clicked Stop");
    }

    public void playControls(View view) {
        createToast("Clicked Play");
    }
}