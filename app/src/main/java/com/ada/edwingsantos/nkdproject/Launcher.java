package com.ada.edwingsantos.nkdproject;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


import com.ada.edwingsantos.nkdproject.glengine.ConeMatrix;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class Launcher extends AppCompatActivity {

    private GLSurfaceView mySurfaceView;
    private ConeMatrix coneMatrix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_launcher);

        if (isOpenGLEnabled()){
            Log.d("OPEN_GL", "is Enabled");
            createOpenGLImage();
        } else {
            Log.d("OPEN_GL", "Not enable on this device");
            finish(); // if not enabled then the application finishes
        }
    }

    /**
     * Checks if Open GL is enabled on the device
     * @return boolean
     */
    private boolean isOpenGLEnabled() {
        ActivityManager am = (ActivityManager)
                getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();
        return info.reqGlEsVersion >= 0x20000;
    }

    /**
     * Create a simple image from a obj file created on Blender.
     */
    private void createOpenGLImage(){

        mySurfaceView = (GLSurfaceView)findViewById(R.id.my_surface_view);
        mySurfaceView.setEGLContextClientVersion(2);


        mySurfaceView.setRenderer(new GLSurfaceView.Renderer() {
            @Override
            public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
                mySurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
                coneMatrix = new ConeMatrix(getApplicationContext());

            }

            @Override
            public void onSurfaceChanged(GL10 gl10, int width, int height) {
                GLES20.glViewport(0,0, width, height);
            }

            @Override
            public void onDrawFrame(GL10 gl10) {
                coneMatrix.draw();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mySurfaceView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mySurfaceView.onPause();
    }


    /**
     * Opens another application and displays a toast message
     * @param view
     */
    public void openC_plus_plus_Call(View view) {
        Toast.makeText(this, "Second Activity Opened.", Toast.LENGTH_LONG).show();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
