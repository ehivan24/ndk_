package com.ada.edwingsantos.nkdproject.glengine;


import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.ada.edwingsantos.nkdproject.R;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConeMatrix {

    private  static final String LOG_TAG = "Cone Matrix";

    private FloatBuffer verticesBuffer;
    private ShortBuffer facesBuffer;
    private List<String> verticesList;
    private List<String> facesList;

    private int mProgram;
    static final int COORDS_PER_VERTEX = 3;
    float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 1.0f };

    private Context mContext;


    public ConeMatrix(Context context) {
        verticesList = new ArrayList<>();
        facesList = new ArrayList<>();
        mContext = context;
    }

    /**
     * Reads a file from the the Raw folder that contains configuration for  the Vertexes
     */
    private void readVertexShaderFile(){

        try {
            InputStream inputStreamVertex = mContext.getResources().openRawResource(R.raw.vertext_shader_code);
            String vertexShaderBuilder = IOUtils.toString(inputStreamVertex, Charset.defaultCharset());
            inputStreamVertex.close();

            int vertexShader = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
            GLES20.glShaderSource(vertexShader, vertexShaderBuilder);

            GLES20.glCompileShader(vertexShader);

            GLES20.glAttachShader(mProgram, vertexShader);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    /**
     * Reads a file from the the Raw folder that contains configuration for  the Fragments
     */
    private void readFragmentShaderFile(){

        try{
            InputStream inputStreamFragment = mContext.getResources().openRawResource(R.raw.fragment_shader_code);
            String fragmentShaderCode = IOUtils.toString(inputStreamFragment, Charset.defaultCharset());
            inputStreamFragment.close();


            int fragmentShader = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
            GLES20.glShaderSource(fragmentShader, fragmentShaderCode);

            GLES20.glCompileShader(fragmentShader);

            GLES20.glAttachShader(mProgram, fragmentShader);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Iterates from a List of coordinates abd returns a ShortBuffer
     *
     * @return ShortBuffer
     */
    private ShortBuffer getFacesFragmentBuffer(){
        ShortBuffer retval = null;

        facesList = getFacesList();

        if (facesList != null) {

            ByteBuffer buffer2 = ByteBuffer.allocateDirect(facesList.size() * 3 * 2);
            buffer2.order(ByteOrder.nativeOrder());
            retval = buffer2.asShortBuffer();

            for (String face : facesList) {
                String vertexIndices[] = face.split(" ");
                short vertex1 = Short.parseShort(vertexIndices[1]);
                short vertex2 = Short.parseShort(vertexIndices[2]);
                short vertex3 = Short.parseShort(vertexIndices[3]);
                retval.put((short) (vertex1 - 1));
                retval.put((short) (vertex2 - 1));
                retval.put((short) (vertex3 - 1));
            }
            retval.position(0);

        } else {

            Log.d(LOG_TAG, " facesList Array is empty");
        }

        return retval;
    }

    /**
     *
     * Iterates from a List of coordinates abd returns a FloatBuffer
     *
     * @return FloatBuffer
     */
    private FloatBuffer getFacesVerticesBuffer(){
        verticesList = getVerticesList();

        FloatBuffer retval = null;

        if (verticesList != null ) {

            ByteBuffer buffer1 = ByteBuffer.allocateDirect(verticesList.size() * 3 * 4);
            buffer1.order(ByteOrder.nativeOrder());
            retval = buffer1.asFloatBuffer();

            for (String vertex : verticesList) {
                String coordinates[] = vertex.split(" ");
                float x = Float.parseFloat(coordinates[1]);
                float y = Float.parseFloat(coordinates[2]);
                float z = Float.parseFloat(coordinates[3]);
                retval.put(x);
                retval.put(y);
                retval.put(z);
            }
            retval.position(0);

        } else {

            Log.d(LOG_TAG, "fragmentList Array is empty");

        }

        return retval;
    }


    /**
     * Reads a file from the Assets folder
     * and puts its content into a List
     * @return List containing the coordinates from a file
     */
    private List<String> getVerticesList(){
        List<String> retval = new ArrayList<>();

        try {
            Scanner scanner = new Scanner(mContext.getAssets().open("cone.obj"));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.startsWith("v ")) {
                    retval.add(line);
                }
            }
            scanner.close();

        }catch (IOException e){
            e.printStackTrace();
        }

        return retval;
    }

    /**
     * Reads a file from the Assets folder
     * and puts its content into a List
     * @return List containing the coordinates from a file
     */
    private List<String> getFacesList() {
        List<String> retval = new ArrayList<>();

        try {
            Scanner scanner = new Scanner(mContext.getAssets().open("cone.obj"));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.startsWith("f ")) {
                    retval.add(line);
                }
            }
            scanner.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return retval;
    }


    /**
     * Generates the openGl Surface
     */
    public void draw(){

        verticesBuffer = getFacesVerticesBuffer();
        facesBuffer = getFacesFragmentBuffer();
        mProgram = GLES20.glCreateProgram();

        readFragmentShaderFile();
        readVertexShaderFile();

        GLES20.glLinkProgram(mProgram);
        GLES20.glUseProgram(mProgram);

        int positionHandler = GLES20.glGetAttribLocation(mProgram, "position");
        GLES20.glEnableVertexAttribArray(positionHandler);
        GLES20.glVertexAttribPointer(positionHandler, 3, GLES20.GL_FLOAT, false, 3 * 4, verticesBuffer);

        float[] projectionMatrix = new float[16];
        float[] viewMatrix = new float[16];
        float[] productMatrix = new float[16];

        Matrix.frustumM(projectionMatrix, 0, -1, 1, -1, 1, 2, 5);

        Matrix.setLookAtM(viewMatrix, 0, 0, 3, -4, 0, 0, 0, 0, 1, 0f);

        Matrix.multiplyMM(productMatrix, 0, projectionMatrix, 0, viewMatrix, 0);

        int matrix = GLES20.glGetUniformLocation(mProgram, "matrix");
        GLES20.glUniformMatrix4fv(matrix, 1, false, productMatrix, 0);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, 3,
                GLES20.GL_UNSIGNED_SHORT, facesBuffer);
        GLES20.glDisableVertexAttribArray(positionHandler);
    }
}
