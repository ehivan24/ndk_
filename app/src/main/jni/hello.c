#include <string.h>
#include <jni.h>
#include <android/log.h>
#include <stdio.h>


JNIEXPORT jstring JNICALL
Java_com_ada_edwingsantos_nkdproject_MainActivity_getMsgFromJni(JNIEnv *env, jobject instance) {



    return (*env)->NewStringUTF(env, "hello");
}

JNIEXPORT jstring JNICALL
Java_com_ada_edwingsantos_nkdproject_MainActivity_getMsgFromJni2(JNIEnv *env, jobject instance) {



    return (*env)->NewStringUTF(env, "New Data");
}

JNIEXPORT jint JNICALL
Java_com_ada_edwingsantos_nkdproject_MainActivity_NativeAdditon(JNIEnv *env, jobject instance, jint pa, jint pb) {

    return pa + pb;
}

JNIEXPORT jstring JNICALL
Java_com_ada_edwingsantos_nkdproject_MainActivity_MemoryAllocation(JNIEnv *env, jobject instance, jint value1,  jint value2){

    char *szFormat = "The sum of the two numbers is: %i";
    char *szResult;

    // add the two values
    jlong sum = value1+value2;

    // malloc room for the resulting string
    szResult = malloc(sizeof(szFormat) + 20);

    // standard sprintf
    sprintf(szResult, szFormat, sum);

    // get an object string
    jstring result = (*env)->NewStringUTF(env, szResult);

    // cleanup
    free(szResult);

    return result;

}




